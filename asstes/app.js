
window.onscroll = function() {scrollFunction()}

function scrollFunction() {
  if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
    document.getElementById("menu__container").style.background = "var(--color-bg)"
    document.getElementById("menu__container").style.minHeight = "4vw"
    document.getElementById("menu__container").style.fontSize = ".8em"
  } else {
    document.getElementById("menu__container").style.removeProperty("background")
    document.getElementById("menu__container").style.minHeight = "var(--nav-height)"
    document.getElementById("menu__container").style.fontSize = "1em"
  }
}


const nav_icon = document.getElementById("nav_icon")
const menu = document.getElementById("menu__container")
nav_icon.addEventListener("click", function(event){
  nav_icon.classList.toggle('active')
  menu.classList.toggle('open')
})


// Fromulario Contacto - send to mail
const formIsValid = () => {
  const nombre = document.querySelector("#name").value
  const email = document.querySelector("#email").value
  const regex = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/

  if (nombre.length === 0) {
    alert("El campo nombre está vacío")
    return false
  }
  if (!regex.test(email)) {
    alert("El campo email es inválido.")
    return false
  }

  return true
}

document.querySelector("form").addEventListener("submit", e => {
  if (!formIsValid()) {
    e.preventDefault()
    return
  }

  let formContacto = document.forms["contacto"]
  let formData = new FormData(formContacto)

  const fetchConfig = {
    method: "POST",
    headers: { "Content-Type": "application/x-www-form-urlencoded" },
    body: new URLSearchParams(formData).toString()
  }
  fetch("/", fetchConfig)
    .then(() => console.log("El mensaje fue enviado exitosamente."))
})